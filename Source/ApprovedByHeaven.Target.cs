using UnrealBuildTool;
using System.Collections.Generic;

public class ApprovedByHeavenTarget : TargetRules
{
	public ApprovedByHeavenTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "ApprovedByHeaven" } );
	}
}
